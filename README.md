# Client-Server FTP

Simple client-server FTP programs that set up a server that allows for up to
10 clients to connect to. The clients cant view the files stored in the server
and also send and retrieve files.

## Installation

To compile the server side files, in the server directory run

```bash
javac Server.java
```

To compile the client side file, in the client directory run

```bash
javac Client.java
```

To run the server run

```bash
java Server
```

To run the client and connect to the server

```bash
java Client
```

## Commands

list = list all files in server<br/>
get 'filename' = retrieve file from server*<br/>
put 'filename' = send file to server**<br/>
bye = disconnect from server<br/>

*Files stored in server are in the serverFiles directory<br/>
**Files stored in client are in the clientFiles directory
