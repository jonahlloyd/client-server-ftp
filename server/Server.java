import java.net.*;
import java.io.*;
import java.util.concurrent.*;
import java.util.*;

/**
*Program to run a server for tansfering files.
*Maximum number of clients is 10.
*
*@author Jonah Lloyd
*/
public class Server{

	/**
	*Main routine of the server.
	*/
	public static void main( String[] args ) throws IOException{

		ServerSocket server = null;
		ExecutorService service = null;

		try{
			// Create new server socket
			server = new ServerSocket(8888);
			System.out.println("\nServer online");
		} catch(IOException e){
				System.err.println("Could not listen on port: 8888");
				System.exit(-1);
		}

		service = Executors.newCachedThreadPool();

		while(true){
			// Accept connection with client and run clientHandler in new thread
			Socket client = server.accept();
			service.submit(new ClientHandler(client));
		}
	}

	public void setMaximumPoolSize(int maximumPoolSize){
		// Set max number of clients connected to server to 10
		maximumPoolSize = 10;
	}
}
