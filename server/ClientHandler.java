import java.net.*;
import java.io.*;
import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
*Handles a client passed from Server.java. Allows communication
*between client and server as well as the ability to transfer
*files both ways.
*
*@author Jonah Lloyd
*/
public class ClientHandler extends Thread {
  private Socket socket = null;

  public ClientHandler(Socket socket) {
	  super("ClientHandler");
	  this.socket = socket;
  }

  /**
  *Run the main routine of the server, contains code for receiving
  *requests and transferring the files.
  *
  *Responds to requests: "list", "get 'fname'", "put 'fname'", "bye"
  */
  public void run() {

    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd:HH:mm:ss");
    // Get current date time with Date()
    Date currentTime = new Date();

    try{

      InetAddress inet = socket.getInetAddress();
      Date date = new Date();

      // Create a new file for logging requests from clients
      PrintWriter writer = new PrintWriter("log.txt", "UTF-8");
      writer.println(dateFormat.format(currentTime) + ":" +
        inet.getHostName() + ":connected");
      writer.flush();

      System.out.println("\nDate " + date.toString() );
      System.out.println("Connection made from " + inet.getHostName() + "\n");

      // Input stream
      BufferedReader in = new BufferedReader(
                            new InputStreamReader(
                              socket.getInputStream()));
      // Output stream
      PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

      String inputLine;

      while((inputLine = in.readLine()) != null){

        // Splitting command into tokens
        String[] tokens = inputLine.split(" ");

        if(tokens[0].equals("list")){

          String currentDirectory = System.getProperty("user.dir");
          String serverFiles = currentDirectory + "/serverFiles";
          File[] files = new File(serverFiles).listFiles();

          String outputLine = "";
          // Append file names to outputLine to be sent to client
          for(File file : files){
            if (file.isFile()){
              outputLine = outputLine + file.getName() + " ";
            }
          }

          out.println(outputLine);
          System.out.println(inet.getHostName() + " requested a list of files");
          writer.println(dateFormat.format(currentTime) + ":" +
            inet.getHostName() + ":[list]requested a list of files");
          writer.flush();
        }

        else if(tokens[0].equals("get")){


          String current = System.getProperty("user.dir");
          String files = current + "/serverFiles/" + tokens[1];
          File file = new File(files);

          // Convert file to datastream
          byte[] fileData = new byte[(int) file.length()];
          BufferedInputStream bis = new BufferedInputStream(
            new FileInputStream(file));
          bis.read(fileData, 0, fileData.length);
          OutputStream os = socket.getOutputStream();

          // Write data to output stream
          os.write(fileData, 0, fileData.length);
          os.flush();

          System.out.println("File sent to " + inet.getHostName());
          writer.println(dateFormat.format(currentTime) + ":" +
            inet.getHostName() + ":[get]requested a file");
          writer.flush();
        }

        else if(tokens[0].equals("put")){

          String current = System.getProperty("user.dir");
          String location = current + "/serverFiles/" + tokens[1];

          //receive data for file and store as new data
          byte[] fileData = new byte[100*1024];
          InputStream is = socket.getInputStream();
          FileOutputStream fos = new FileOutputStream(location);
          BufferedOutputStream bos = new BufferedOutputStream(fos);
          int bytesRead = is.read(fileData, 0, fileData.length);

          bos.write(fileData, 0, bytesRead);
          bos.flush();
          bos.close();

          System.out.println("File receieved from " + inet.getHostName());
          writer.println(dateFormat.format(currentTime) + ":" +
            inet.getHostName() + ":[put]sent a file");
          writer.flush();

        }

        else if(tokens[0].equals("bye")){
          break;
        }
      }

      in.close();
      out.close();
      socket.close();
      System.out.println(inet.getHostName() + " disconnected\n");
      writer.println(dateFormat.format(currentTime) + ":" +
        inet.getHostName() + ":[bye]disconnected");
      writer.flush();
    } catch (IOException e){
        InetAddress inet = socket.getInetAddress();
        System.err.println("\nClient connection lost from " +
          inet.getHostName());
    }
  }
}
