import java.util.*;
import java.net.*;
import java.io.*;

/**
*Client side program for connecting to server and sending requests.
*Able to transfer files both ways.
*
*@author Jonah Lloyd
*/
public class Client{
  /**
  *Main routine of client side program.
  */
  public static void main( String[] args ){
    ClientConnection connection = new ClientConnection();
    connection.Communicate();
  }
}

/**
*Class containing method for communicating with server.
*
*@author Jonah Lloyd
*/
class ClientConnection{

  private Socket clientSocket = null;
  private PrintWriter socketOut = null;
  private BufferedReader socketIn = null;
  private Scanner keyboardReader = null;

  /**
  *Method for creating sockets and passing data between
  *client and server.
  */
  public void Communicate(){

    try{
      //Try to create socket on localhost
      clientSocket = new Socket( "localhost", 8888);
      //Write Stream
      socketOut = new PrintWriter(clientSocket.getOutputStream(), true);
      //Read Stream
      socketIn = new BufferedReader(new InputStreamReader(
        clientSocket.getInputStream()));


    } catch(UnknownHostException e){
      System.err.println("Unknown Host");
      System.exit(1);

    } catch(IOException e){
      System.err.println("Failed connecting to host");
      System.exit(1);
    }

    keyboardReader = new Scanner(System.in);

    while(true){
      System.out.println("\nEnter Command:\t[Enter 'bye' to disconnect]");
      //Reading line from stdin
      String cmd = keyboardReader.nextLine();
      //Splitting command into tokens
      String[] tokens = cmd.split(" ");

      if(tokens[0].equals("list")){
        try{
          //send request to server to return a list
          socketOut.println("list");
          socketOut.flush();
          //read stream from server and store as variable
          String list = socketIn.readLine();

          if(list != null){
            System.out.println("Files on Server: " + list);
          } else{
            System.err.println("No files found");
          }
          socketOut.flush();

        } catch(IOException e){
          System.err.println("Unable to retrieve files");
        }
      }

      if(tokens[0].equals("get")){
        if(tokens.length == 2){
          try{
            //send request to server with file name
            socketOut.println("get" + " " + tokens[1]);
            socketOut.flush();

            //InputStream recieves returned file
            byte[] fileData = new byte[100*1024];
            InputStream is = clientSocket.getInputStream();
            //find current directory and store as variable
            String current = System.getProperty("user.dir");
            String location = current + "/clientFiles/" + tokens[1];
            //create a new file for the received file data
            FileOutputStream fos = new FileOutputStream(location);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            int bytesRead = is.read(fileData, 0, fileData.length);
            //write to the file
            bos.write(fileData, 0, bytesRead);
            bos.flush();
            bos.close();
            System.out.println("File recieved");

          } catch(IOException e){
            System.err.println("Unable to retrieve files");
          }
        } else if (tokens.length == 1){
            System.err.println("A file name is required");
        } else {
            System.err.println("Only enter one file name");
        }
      }

      if(tokens[0].equals("put")){
        if(tokens.length == 2){
          try{
            //send request to send a file
            socketOut.println("put" + " " + tokens[1]);
            socketOut.flush();

            String current = System.getProperty("user.dir");
            String files = current + "/clientFiles/" + tokens[1];
            File file = new File(files);

            //convert file into data stream
            byte[] fileData = new byte[(int) file.length()];
            BufferedInputStream bis = new BufferedInputStream(
              new FileInputStream(file));
            bis.read(fileData, 0, fileData.length);
            OutputStream os = clientSocket.getOutputStream();

            os.write(fileData, 0, fileData.length);
            os.flush();
            System.out.println("File sent");

          } catch(IOException e){
            System.err.println("Unable to retrieve files");
          }
        } else if(tokens.length == 1){
          System.err.println("A filename is required");
        } else{
          System.err.println("Only enter on file name");
        }
      }

      if(tokens[0].equals("bye")){
        socketOut.println("bye");
        socketOut.flush();
        System.exit(0);
      }
    }
  }
}
